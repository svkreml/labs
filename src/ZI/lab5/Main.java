package ZI.lab5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Dropper dropper = new Dropper();
        dropper.start();
        new Thread(()->{
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dropper.finish();
        }).start();
    }
}
