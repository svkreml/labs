package ZI.lab5;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by svkreml on 27.10.2016.
 */
public class Dropper {
    Queue<Message> buffer = new ArrayBlockingQueue<Message>(1024);
    Thread producerThread;
    Thread consumerThread;
    AtomicLong counter = new AtomicLong();
    AtomicLong counterWrong = new AtomicLong();
    AtomicLong counterDropped = new AtomicLong();
    AtomicLong counterTimeout = new AtomicLong();
    boolean stop;

    public void finish() {

        stop = false;
        double counterDropped = this.counterDropped.get();
        double counter = this.counter.get();
        double counterWrong = this.counterWrong.get();
        double counterTimeout = this.counterTimeout.get();
        double dropped = counterDropped / counter;
        double wrong = counterWrong / counter;
        double timeout = counterTimeout / counter;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println("wrong = " + wrong + " dropped = " + dropped + " timeout = " + timeout);
    }


    public void start() {
        stop = true;
        AtomicLong x = new AtomicLong();
        new Thread(() -> {
            while (true)
                if (x.get() > 0) {
                    System.out.println("timeout");
                    x.decrementAndGet();
                    if(x.get()==0&&!stop)
                        break;
                }
        }).start();
        consumerThread = new Thread(() -> {

            while (stop) {
                try {
                    Message msg = buffer.remove();
                    if (new Date().getTime() - msg.time > 7) {
                        counterTimeout.incrementAndGet();
                        x.incrementAndGet();
                        continue;
                    }
                    if (msg.line.charAt(0) == 'a' || msg.line.charAt(0) == 'b') {
                        System.out.println("message wrong");
                        counterWrong.incrementAndGet();
                        continue;
                    }
                    System.out.println(buffer.size() + " " + msg.line);
                } catch (NoSuchElementException e) {
                    System.out.println("queue empty");
                }
            }

        });
        consumerThread.start();


        producerThread = new Thread(() -> {

            while (stop) {

                try {
                    counter.incrementAndGet();
                    buffer.add(new Message(UUID.randomUUID().toString()));
                } catch (IllegalStateException e) {
                    counterDropped.incrementAndGet();
                    System.out.println("queue full");
                }
            }

        });
        producerThread.start();

    }


}
