package ZI.lab5;

import java.util.Date;

/**
 * Created by svkreml on 28.10.2016.
 */
public class Message {
    String line;
    long time;

    public Message(String line) {
        this.line = line;
        time = new Date().getTime();
    }
}
